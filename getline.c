ssize_t getline(char **buffer, size_t *n, FILE *fp){
	char *buff = *buffer;
	size_t buff_size = *n;

	ssize_t length = 0;

	if(buff == NULL && buff_size == 0){
		buff_size = 16;
		buff = new char[buff_size];
	}

	while(true){
		int character = fgetc(fp);

		if(character == EOF){
			return -1;
		}

		char c = character;

		if(buff_size <= length + 1){
			char* new_buff = new char[buff_size * 2];
			memcpy(new_buff, buff, buff_size);
			delete buff;
			buff_size *= 2;
			buff = new_buff;
		}
		buff[length] = c;
		length += 1;

		if(c == '\n' || c == '\r'){
			break;
		}
	}

	// add null terminator
	buff[length + 1] = 0;

	*buffer = buff;
	*n = buff_size;
	return length;
}